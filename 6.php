<?php

echo("<br>");

echo("<h3>Normal sorted list of Strings with recurring elements!</h3>");

$num =array("john","john","miguel","miguel");
sort($num);
print_r($num);

echo("<br>");
echo("<br>");

echo("<h3>After deleting the recurring elements inside a sorted list of Strings!</h3>");

$result = array_unique($num);

print_r($result);

?>
